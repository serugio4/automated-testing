//
//  mainPage.swift
//  AimForThat2017
//
//  Created by sergio garcia perez on 23/12/19.
//  Copyright © 2019 Frogames. All rights reserved.
//


import XCTest

class MainScreen: BaseTest {
    
    //locators
    
    let infoButtonLocator = "infoButton"
    
    //elements
    lazy var infoButton: XCUIElement = app.buttons[infoButtonLocator]
        
    
    //functions
    
    func tapInfoButton(){
        
        infoButton.tap()
        
    }
    
    
}

 
 
