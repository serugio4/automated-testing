//
//  aboutPage.swift
//  AimForThat2017
//
//  Created by sergio garcia perez on 24/12/19.
//  Copyright © 2019 Frogames. All rights reserved.
//

import XCTest


class AboutScreen: BaseTest {
    
    //locators
    
    let aboutLabelLocator = "★★★ Aim For That 2017 ★★★"
    
    //elements
    
    lazy var aboutLabel: XCUIElement = app.webViews.firstMatch.staticTexts["\(aboutLabelLocator)"]
    
    
    //functions
    
    func aboutLabelExists() -> Bool {
        
        _ = aboutLabel.waitForExistence(timeout: TimeInterval(10))
        return aboutLabel.exists
        
    }
    
    
}
