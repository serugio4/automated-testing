//
//  stepsImplementation.swift
//  AimForThat2017
//
//  Created by sergio garcia perez on 23/12/19.
//  Copyright © 2019 Frogames. All rights reserved.
//

import XCTest
import XCTest_Gherkin


class stepDefinition: StepDefiner {
    
    let app = XCUIApplication()
    var mainScreen = MainScreen()
    var aboutScreen = AboutScreen()
    var baseTest = BaseTest()
    

    
    override func defineSteps() {
        step("no preconditions required"){
            // Setting continue after failure strategy
            
            // Launching the app
            self.app.launch()
            self.mainScreen.printTestTree()
        }
        
        step("tap info button"){
            
            self.mainScreen.tapInfoButton()
    }
    
        step("should appear game description"){
            
            XCTAssertTrue(self.aboutScreen.aboutLabelExists())
        }
    
        
    }
}

 
 
