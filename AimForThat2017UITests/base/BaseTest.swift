//
//  BaseTest.swift
//  AimForThat2017UITests
//
//  Created by sergio garcia perez on 2/01/20.
//  Copyright © 2020 Frogames. All rights reserved.
//

import Foundation
import XCTest


class BaseTest: XCTestCase {
    
    let app = XCUIApplication()
    
    override func setUp() {
        continueAfterFailure = false
        app.launch()
    }
    
    override func tearDown() {
        let screenshot = XCUIScreen.main.screenshot()
        let fullScreenshotAttachment = XCTAttachment(screenshot: screenshot)
        fullScreenshotAttachment.lifetime = .deleteOnSuccess
        add(fullScreenshotAttachment)
        app.terminate()
    }
    
    func takeScreenshotOfWindow() {
        let screenshot = app.windows.firstMatch.screenshot()
        let attachment = XCTAttachment(screenshot: screenshot)
        attachment.lifetime = .deleteOnSuccess
        add(attachment)
    }
    
    // utils
    
    func printTestTree(){
        print(app.debugDescription)
    }
}
